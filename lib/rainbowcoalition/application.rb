require 'sinatra'
require 'sinatra/cookies'
require 'slim'
require 'tilt/kramdown'
require 'rainbowcoalition/creds'
require 'rainbowcoalition/user'
require 'rainbowcoalition/ldap'
require 'rainbowcoalition/mail'
require 'rainbowcoalition/postgres'
require 'rainbowcoalition/page'
require 'rainbowcoalition/cookies'
require 'rainbowcoalition/kramdown'

module RainbowCoalition
  class Application < Sinatra::Base

    SIDEBARS = {}

    @@pages = {}
    @@run_hooks = []

    private_class_method def self.define_helper(name)
      define_method(name) do |&blk|
        template(name, &blk)
      end
      private name
    end
    %i[ article header ].each { |name| define_helper(name) }

    private_class_method def self.on_run(&blk)
      @@run_hooks << blk
    end

    def self.hooks
      proc do |*args|
        @@run_hooks.each do |hook|
          hook[*args]
        end
      end
    end

    def self.page(name)
      @@pages[name] ||= Page.new("#{root}/pages/#{name}.slim", name)
    end

    def page(name)
      RainbowCoalition::Application.page(name)
    end

    def sidebar_links(group)
      SIDEBARS[group]&.select do |link|
        case link[2]
        when :user
          !current_user.nil?
        when :admin
          current_user&.admin?
        else
          true
        end
      end
    end


    include RainbowCoalition::User
    include RainbowCoalition::Ldap
    include RainbowCoalition::Mail
    include RainbowCoalition::Postgres

    helpers Sinatra::Cookies
    helpers RainbowCoalition::Cookies
    use Rack::TempfileReaper

    set :root, File.dirname(File.dirname(File.dirname(__FILE__)))
    set :public_folder, "#{root}/static"
    set :views, "#{root}/views"
    set :protection, except: %i(path_traversal session_hijacking)

    Tilt.register Tilt::KramdownTemplate, 'md'

    set :server, config.dig('webserver', 'server') || "puma"
    set :server_settings, config.dig('webserver', 'server_settings') || {}
    settings.server_settings.transform_keys! {|k| k.to_sym}
    set :quiet, config.dig('webserver', 'quiet') || false

    config.dig('webserver', 'address')&.then {|address|
      set :bind, address
    }

    config.dig('webserver', 'port')&.then {|port|
      set :port, port
    }

    if (config.dig('webserver', 'serve_static') || false)
      set :static, true
    else
      set :static, false
      use Rack::Sendfile
    end

    Slim::Engine.set_options(:pretty => (config.dig('slim', 'pretty') || false))

    if config.dig('page', 'reload')
      class ::RainbowCoalition::Page
        def body
          if File.mtime(@file) > @rendered_at
            render!
          end

          @body
        end
      end
    end

    if settings.server == 'webrick'
      on_run do |server|
        Rack::Mime::MIME_TYPES.merge!(server.config[:MimeTypes].transform_keys {|k| ".#{k}"})
      end
    end

    if settings.server == 'puma'
      on_run do |server|
        server.config.configure do |config, _|
          config.before_fork do
            RainbowCoalition::Postgres.database.disconnect
          end

          config.log_requests

          config.set_remote_address header: 'X-Forwarded-For'
        end
      end
    end

    begin
      controllers_path = "#{root}/lib/rainbowcoalition/controllers"

      Dir.glob('*.rb', base: controllers_path).each do |controller|
        controller_path = File.join(controllers_path, controller)
        next unless File.file? controller_path

        puts "Loading #{File.basename(controller, '.rb')} controller"
        require controller_path
      end
    end

    begin
      Dir.glob('**/*.slim', base: "#{root}/pages") do |page_name|
        page_name.delete_suffix!('.slim')
        page_path = "/#{page_name}"
        page = page(page_name)

        case page.access
        when :user
          get page_path do
            redirect "/login?redirect_to=#{Rack::Utils.escape(page_path.sub(/^\//, ''))}", 303 unless current_user

            slim(:page) do
              page
            end
          end
        when :admin
          get page_path do
            halt 404 unless current_user&.admin?

            slim(:page) do
              page
            end
          end
        else
          get page_path do
            slim(:page) do
              page
            end
          end
        end
      end
    end

    get '/' do
      slim(:page) do
        self.class.page('index')
      end
    end

    error 400..599 do
      if !response.body || response.body.empty?
        response.body = slim(:error)
      end
    end

    def post_errors
      @post_errors ||= Hash.new {|hash, key| hash[key] = []}
    end

    def template(name, options = {}, *args, &blk)
      options[:views] = "#{self.class.root}/templates"
      slim(name.to_sym, options, *args, &blk)
    end

    private def nil_s(string)
      return nil if string.nil? || string.empty?
      string
    end

    private def input(name, label, type = 'text', clarification = nil, **kwargs)
      template("input", {}, { name: name, label: label, type: type, clarification: clarification, kwargs: kwargs })
    end

    private def preamble
      @@preamble ||= File.read("#{self.class.root}/templates/preamble.html")
    end

    private def deconstruct_navigation_link(link)
      title = link[0]
      i = 1

      if link[i].is_a? String
        uri = link[i]
        i += 1
      end

      if link[i].is_a? Array
        sublinks = link[i]
        i += 1
      end

      if link[i].is_a? Hash
        kwargs = link[i]
      end

      [title, uri, sublinks || [], kwargs || {}]
    end

    private def navigation_links
      return @navigation_links if @navigation_links
      @navigation_links = []
      @navigation_links << ["Chat", "https://chat.rainbowcoalition.net", {target: '_blank'}] if current_user

      @navigation_links.append *[
        ["Community", "/community", [
          ["Rules", "/rules"],
          ["Philosophy", "/philosophy"],
          ["FAQ", "/faq"],
          ["Contact", "/contact"],
        ]],
        ["Arma", "/arma", [
          ['Rules', '/arma/rules'],
          ['FAQ', '/faq#arma'],
          ['Glossary', '/arma/glossary'],
          ['Resources', '/arma/resources']
        ]],
        ["Activities", "/activities", [
          ["Book Club", "/books"],
          ["Movie Night", "/movies"]
        ]],
        ["Games", "/games", [
          ["Arma", "/arma"],
          ["Barotrauma", "/barotrauma"],
          ["Factorio", "/factorio"],
          ["Lethal Company", "/lethalcompany"],
          ["Project Zomboid", "/zomboid"],
          ["Tabletop Simulator", "/tts"],
          ["Valheim", "/valheim"]
        ]]
      ]

      if current_user
        @navigation_links.detect {|_, link, _| link == "/arma"}[2] = [
          ['Rules', '/arma/rules'],
          ['FAQ', '/faq#arma'],
          ['Introduction', '/arma/onboarding'],
          ['Mods', '/arma/mods'],
          ['Glossary', '/arma/glossary'],
          ['Resources', '/arma/resources'],
          ['Jobs', '/arma/jobs']
        ]

        @navigation_links << ["Member", "/member", [
          ["Intro", "/intro"],
          ["Profile", "/profile"],
          ["Directory", "/profiles"]
        ]]

        if current_user.admin?
          @navigation_links << ["Admin", "/admin", [
            ["Signups", "/signups"],
            ["Onboard", "/onboard"],
          ]]
        end
      else
        @navigation_links << ["Sign Up", "/signup"]
      end

      @navigation_links.map! {|link| deconstruct_navigation_link(link)}
    end

    # Modified from Sinatra's send_file, which is deficient in a couple ways
    private def send_file(path, opts = {})
      disposition = opts[:disposition]
      filename    = opts[:filename]
      disposition = :attachment if disposition.nil? && filename
      filename    = path        if filename.nil?
      attachment(filename, disposition) if disposition

      last_modified opts[:last_modified] if opts[:last_modified]

      file   = Rack::Files.new(settings.root)

      result = file.serving(request, path)

      if opts[:type] || !response['Content-Type']
        content_type File.extname(path), default: 'application/octet-stream'
      end

      result[1].each { |k, v| headers[k] ||= v }
      headers['Content-Length'] = result[1]['Content-Length']
      opts[:status] &&= Integer(opts[:status])
      halt (opts[:status] || result[0]), result[2]
    rescue SystemCallError => e
      response.status = 404
      response.body = NotFoundFile.new(slim(:error), path)
      halt 404
    end

    class NotFoundFile < Array
      attr_reader :path
      def initialize(body, path)
        super([body])
        @path = path
      end

      def to_path
        path
      end
    end
  end
end
