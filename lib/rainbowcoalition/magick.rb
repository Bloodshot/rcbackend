require 'rmagick'

module RainbowCoalition
  module Magick
    def self.to_face(image_bytes)
      #::Magick.limit_resource(:time, 10)
      image = ::Magick::Image.from_blob(image_bytes).first.then {|image|
        image.resize_to_fit(256, 256)
      }
      { jpg: 'JPEG', png: 'PNG' }.transform_values { |format|
        image.to_blob {|i| i.format = format }
      }
    rescue => e
      warn e.full_message
      { error: e }
    end
  end
end
