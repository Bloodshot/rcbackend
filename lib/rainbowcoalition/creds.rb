module RainbowCoalition
  module Creds
    def self.load_cred(name)
      env = ENV[name.tr('a-z', 'A-Z')]
      return env if env

      creds_dir = ENV['CREDENTIALS_DIRECTORY']
      return File.read(File.join(creds_dir, name)) if creds_dir

      nil
    rescue
      nil
    end
  end

  class Cred < String
    def self.new(name)
      c = Cred.allocate
      c.send(:initialize, Creds.load_cred(name)).freeze
      c
    end

    # Undocumented Psych behaviour:
    #
    # When deserializing a YAML document, mappings tagged with
    # ruby/object:Klass will be allocated (not initiailized)
    # and have their instance variables set to the values of the map.
    #
    # However, if Klass implements #init_with, the allocated instance is
    # instead called with a Psych::Coder object whose map variable is set to
    # the YAML map, and the result is the return value of Klass#init_with.
    #
    # See lib/psych/visitors/to_ruby.rb
    def init_with(coder)
      name = coder.map["path"] || coder.map["name"]
      initialize(Creds.load_cred(name))
      freeze
      self
    end
  end
end
