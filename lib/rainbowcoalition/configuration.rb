require 'sinatra'
require 'yaml'
require 'rainbowcoalition/creds'

module RainbowCoalition
  class Application < Sinatra::Base
    def self.load_configuration(config)
      @@configuration = YAML.safe_load_file(config, permitted_classes: [RainbowCoalition::Cred])
    end

    def self.config
      @@configuration
    end

    def config
      @@configuration
    end
  end
end
