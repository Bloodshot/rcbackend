require 'securerandom'

module RainbowCoalition
  module Password
    def self.hash_password(pass)
      # Parameters taken to match my own /etc/shadow
      pass.crypt("$y$jAT$#{salt}$")
    end

    def self.salt
      SecureRandom.alphanumeric(32)
    end

    def self.ldap_password(pass)
      "{CRYPT}#{hash_password(pass)}"
    end
  end
end
