module Kramdown
  module Converter
    class Html

      # Modified from the source in a couple ways:
      #
      # Takes a caption option if the figcaption should be different than the alt text
      # <figcaption> is only added if caption is specified. If caption option is present
      #   but empty, use the alt text
      # Takes a comma separated list of types. If specified, use <picture> and srcset
      #
      def convert_standalone_image(el, indent)
        figure_attr = el.attr.dup
        image_attr = el.children.first.attr.dup

        figure_attr['class'] = image_attr.delete('class') if image_attr.key?('class') and not figure_attr.key?('class')
        figure_attr['id'] = image_attr.delete('id') if image_attr.key?('id') and not figure_attr.key?('id')

        options = el.children.first.options[:ial].dup

        if options[:refs]&.include? 'caption'
          caption = image_attr['alt']
        else
          caption = image_attr.delete('caption')
        end

        caption &&= "#{' ' * (indent + @indent)}<figcaption>#{caption}</figcaption>\n" unless caption&.empty?
        caption ||= ''

        types = image_attr.delete('types')&.split(',')
        if types && !types.empty?
          imgtype = types.delete_at(-1)
          src = image_attr["src"].sub(/\.[a-zA-Z0-9]*$/, "")

          picture = "#{' ' * (indent + @indent)}<picture>\n#{
            types.map {|type|
              "#{' ' * (indent + @indent * 2)}<source srcset=\"#{src}.#{type}\" type=\"image/#{type}\" />\n"
            }.join('')
          }#{
            "#{' ' * (indent + @indent * 2)}<img#{html_attributes(image_attr)} />\n"
          }#{' ' * (indent + @indent)}</picture>\n"
        else
          picture = "#{' ' * (indent + @indent)}<img#{html_attributes(image_attr)} />\n"
        end

        body = "#{picture}#{caption}"
        format_as_indented_block_html("figure", figure_attr, body, indent)
      end
    end
  end
end
