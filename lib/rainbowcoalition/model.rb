module RainbowCoalition
  class Model
    private_class_method def self.inherited(model)
      model.class_exec do
        @attribute_name_map = {}
        @attributes = []
        @validations = Hash.new {|hash, key| hash[key] = []}
      end
    end

    private_class_method def self.attributes(*attributes)
      attributes = Model.coerce(attributes)
      attribute_name_map = Hash[attributes.map {|attribute_name|
        [Model.snake_case(attribute_name).to_sym, attribute_name]
      }]

      attr_reader *attribute_name_map.keys
      attribute_name_map.keys.each do |attribute|
        define_method(:"#{attribute}=") do |value|
          validate_and_set(attribute, value)
        end
      end

      @attribute_name_map.merge! attribute_name_map
      @attributes.concat attribute_name_map.keys
    end

    def self.attribute_names
      @attributes
    end

    def self.attribute_name_map
      @attribute_name_map
    end

    private_class_method def self.validate(attribute = nil, &blk)
      attr = snake_case(attribute).to_sym if attribute
      @validations[attr] << blk
    end

    private_class_method def self.must_have(*attributes)
      attributes.each do |attribute|
        attr = snake_case(attribute).to_sym
        validate(attr) {|attr| !attr.nil?}
      end
    end

    def self.snake_case(string)
      # Taken from ActiveSupprt::Inflector#underscore
      return string unless /[A-Z-]|::/.match?(string)
      word = string.to_s.gsub("::".freeze, "/".freeze)
      word.gsub!(/([A-Z\d]+)([A-Z][a-z])/, '\1_\2'.freeze)
      word.gsub!(/([a-z\d])([A-Z])/, '\1_\2'.freeze)
      word.tr!("-".freeze, "_".freeze)
      word.downcase!
      word
    end

    def self.coerce(args)
      args.flat_map(&:itself)
    end

    def validate!
      attributes.each do |attribute|
        value = send(attribute)
        validate_attribute!(attribute, value)
      end

      validate_attribute!(nil, self)
    end

    private def validate_attribute!(attribute, value)
      self.class.instance_variable_get('@validations')[attribute].each do |validation|
        unless validation[value]
          if attribute
            raise ValidationError, "Invalid #{attribute} (#{value.inspect}): #{validation.source_location.join(':')}"
          else
            raise ValidationError, "#{self.class.to_s.rpartition(':').last} failed Validation: #{validation.source_location.join(':')}"
          end
        end
      end
    end

    private def validate_and_set(attribute, value)
      validate_attribute!(attribute, value)
      instance_variable_set("@#{attribute}", value)
      validate_attribute!(nil, self)

      value
    end

    def initialize(hash = nil, **kwargs)
      raise ArgumentError if (hash && (!kwargs.empty?))
      (hash || kwargs).each_pair do |key, value|
        if attributes.include? key
          instance_variable_set("@#{key}", value)
        else
          unrecognized_attribute(key)
        end
      end

      after_initialize if respond_to? :after_initialize
      validate!
    end

    def <<(other)
      initialize(other.to_h)

      self
    end

    def ==(other)
      attributes.all? do |attribute|
        other.public_send(attribute) == attribute
      end
    end

    def -(other)
      to_h.select do |attribute, value|
        other.public_send(attribute) != send(attribute)
      end
    end

    def to_h
      Hash[attributes.map {|attr| [attr, send(attr)]}]
    end

    def to_s
      attr_string = attributes.
        select {|attr| !send(attr).nil? }.
        map {|attr| "#{attr}: #{send(attr)}" }.
        join(', ')
      "#<#{self.class.name} #{attr_string}>"
    end

    def inspect
      to_s
    end

    def unrecognized_attribute(key) ; end

    def attributes
      self.class.attribute_names
    end

    private def attribute_name_map
      self.class.instance_variable_get('@attribute_name_map')
    end

    class ValidationError < StandardError ; end
  end
end
