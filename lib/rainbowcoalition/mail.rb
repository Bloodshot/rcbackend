module RainbowCoalition
  module Mail
    def self.from_header
      address = RainbowCoalition::Application.config.dig('mail', 'address') || 'contact@rainbowcoalition.net'
      "Rainbow Coalition <#{address}>".freeze
    end

    def self.send_loop
      warn "Starting e-mail loop"
      loop { send_mail(*Ractor.receive) }
    end

    def self.send_mail(address, subject, body)
      IO.popen([
                'mail',
                '-a', "FROM:#{@from_header}",
                '-s', subject, address
               ], "r+") {|io|
        io.write(body)
        io.close_write
        io.read
      }.then {|output|
        if output && !output.empty?
          warn "`mail` has output a warning:\n#{output}\n\n"\
               "while trying to send to #{address}"
        end
      }
    end

    private_class_method :send_loop, :send_mail


    def self.mail_ractor
      unless RainbowCoalition::Application.config['mail']&.fetch('enabled', true)
        return
      end

      @from_header = from_header

      @@mail_ractor ||= Ractor.new(name: 'mail-ractor') { ::RainbowCoalition::Mail.send(:send_loop) }
    end

    def email!(*args)
      Mail.mail_ractor&.send(args)
    end

    def email(*args)
      email!(*args)
    rescue => e
      # Don't fail the request on a failed email send
      warn e.full_message
    end

    def email_template(address, subject, *args)
      email(address, subject, template(*args))
    end
  end
end
