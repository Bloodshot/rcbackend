require 'rainbowcoalition/postgres'

module RainbowCoalition
  class Model
    class SignupRequest < Sequel::Model
      unrestrict_primary_key

      def approved?
        status == 'approved'
      end

      def rejected?
        status == 'soft_rejected' || status == 'hard_rejected'
      end

      def pending?
        status == 'pending'
      end
    end
  end
end
