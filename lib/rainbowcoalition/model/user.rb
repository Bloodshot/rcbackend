require 'rainbowcoalition/ldap_model'
require 'rainbowcoalition/ldap'

module RainbowCoalition
  class Model
    class User < LdapModel
      PUBLIC_ATTRIBUTES = %w(displayName uid sn cn givenName title description jpegPhoto employeeNumber)
      PRIVATE_ATTRIBUTES = %w(userPassword mail)

      attributes PUBLIC_ATTRIBUTES
      attributes PRIVATE_ATTRIBUTES
      binary_attributes %w(jpegPhoto)
      array_attributes :memberOf

      must_have :cn, :uid
      validate :userPassword do |password|
        next true unless password

        password.start_with?('{CRYPT}$') &&
          password.size > 60
      end

      validate(:cn) {|cn| !cn.empty?}
      validate(:uid) {|uid| !uid.empty?}

      def self.public_user_attributes
        PUBLIC_ATTRIBUTES
      end

      def after_initialize
        self.dn ||= "uid=#{Ldap.ldap_escape(uid)},#{Ldap.ldap_people_dn}"
        self.object_class ||= []
        self.object_class |= %w(inetOrgPerson organizationalPerson person top)
      end

      def friendly_name
        display_name || (cn&.strip) || uid
      end

      def groups
        return [] unless member_of
        @groups ||= member_of.map {|dn|
          dn.sub(/^cn=([^,]+),#{Regexp.escape(Ldap.ldap_group_dn)}$/, '\1')
        }.select {|group| !group.nil?}
      end

      def member?
        groups.include? 'communityMember'
      end

      def admin?
        groups.include? 'admin'
      end

      def inactive?
        groups.include? 'inactive'
      end
    end
  end
end
