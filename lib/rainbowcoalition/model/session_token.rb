require 'rainbowcoalition/postgres'
require 'securerandom'
require 'base64'

module RainbowCoalition
  class Model
    class SessionToken < Sequel::Model
      unrestrict_primary_key

      def self.create_for(user)
        now = SessionToken.now
        create(user: user,
               token: SecureRandom.urlsafe_base64(30),
               created_at: now,
               last_used: now)
      end

      def validate
        super
        errors.add(:last_used, 'last_used must be later than creation') if created_at > last_used
        errors.add(:user, 'user cannot be empty') if user.empty?
        begin
          Base64.urlsafe_decode64(token)
        rescue ArgumentError
          errors.add(:token, 'invalid token')
        end
      end

      def expired?
        (SessionToken.now - created_at) > 60 * 60 * 24 * 28 || # Four weeks
          (SessionToken.now - last_used) > 60 * 60 * 24 * 7 # One week
      end

      def self.now
        Time.at(Process.clock_gettime(Process::CLOCK_TAI))
      end
    end
  end
end
