module RainbowCoalition
  module Cookies
    private def add_cookie(key, value, **kwargs)
      value = { value: value } unless value.is_a? Hash
      value = value.merge(kwargs)

      Rack::Utils.set_cookie_header!(headers, key, value)
    end

    private def delete_cookie(key, **kwargs)
      Rack::Utils.delete_cookie_header!(headers, key, kwargs)
    end

    private def secure_cookie(cookie_name)
      cookie_name = cookie_name.sub('__Secure-', '').sub('__Host-', '') if self.class.development?

      cookies[cookie_name]
    end

    private def set_secure_cookie(key, value, **kwargs)
      kwargs[:path] = '/'
      kwargs[:http_only] = true
      kwargs[:same_site] = :lax

      if self.class.development?
        # Localhost is not HTTPS
        kwargs[:secure] &&= false
        key = key.sub('__Secure-', '').sub('__Host-', '')
      else
        kwargs[:secure] = true
      end

      add_cookie(key, value, **kwargs)
    end

    private def delete_secure_cookie(cookie_name)
      cookie_name = cookie_name.sub('__Secure-', '').sub('__Host-', '') if self.class.development?

      delete_cookie(cookie_name, secure: !self.class.development?)
    end
  end
end

module Sinatra
  module Cookies
    class Jar
      # The upstream implementation does not handle arrays of cookies
      def parse_response
        string = @response['Set-Cookie']
        return if @response_string == string

        hash = {}

        Array(string).flat_map(&:lines).each do |line|
          key, value = line.split(';', 2).first.to_s.split('=', 2)
          next if key.nil?
          key = Rack::Utils.unescape(key)
          if line =~ /expires=Thu, 01[-\s]Jan[-\s]1970/
            @deleted << key
          else
            @deleted.delete key
            hash[key] = value
          end
        end

        @response_hash.replace hash
        @response_string = string
      end
    end
  end
end
