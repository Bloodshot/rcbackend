require 'rainbowcoalition/postgres'
require 'rainbowcoalition/model/signup_request'
require 'rainbowcoalition/model/user'
require 'net/http'
require 'uri'
require 'securerandom'

module RainbowCoalition
  module Controllers
    module Signup
      def signup_request
        @signup_request ||= begin
          Model::SignupRequest[signup_token] if signup_token
        end
      end

      def signup_token
        secure_cookie '__Host-signup_token'
      end

      def user_exists?(username)
        with_ldap_admin do |l|
          ldap_name_taken = !l.search(Ldap.ldap_people_dn,
                                      "uid=#{ldap_escape(username)}",
                                      attributes: 'uid',
                                      size: 1).empty?
          postgres_name_taken = false
          unless ldap_name_taken
            postgres_name_taken = Model::SignupRequest.where(username: username).where(status: 'pending').count > 0
          end

          ldap_name_taken || postgres_name_taken
        end
      end

      def verify_params
        post_errors['username'] << 'Missing username' unless params[:username]
        post_errors['email'] << 'Email too large' if params[:email] && params[:email].size > 1024
        # Senior developer procedure to validate e-mail
        post_errors['email'] << 'Not an email' if params[:email] && !params[:email].empty? && !params[:email].include?('@')
        if !params['empty-email'] && (params['email'].nil? || params['email'].empty?)
          post_errors['email'] << 'No email provided. ' \
                                  'If you are sure you do not want to be notified via email, please submit again.'
          @empty_email = true
        end

        post_errors['username'] << 'Invalid username' if !valid_username?(params[:username])
        post_errors['description'] << 'Description too large' if params[:description] && params[:description].size > 16384
      end

      def generate_token
        SecureRandom.alphanumeric(48)
      end

      private def next_user_number(ldap)
        ldap.search(Ldap.ldap_people_dn, 'uid=*', attributes: %w(employeeNumber)).map {|result|
          result["employeeNumber"][0].to_i
        }.max + 1
      end

      def approve_request(request)
        new_user = Model::User.new(
          uid: request.username,
          cn: request.username,
          mail: nil_s(request.email),
          sn: request.username
        )

        with_ldap_admin do |l|
          new_user.employee_number = next_user_number(l).to_s

          begin
            l.create(new_user)
          rescue LDAP::Error => e
            warn e.full_message
            halt(500, "Failed to create user")
          end

          begin
            l.modify("cn=communityMember,#{Ldap.ldap_group_dn}",
                     { 'member' => [new_user.dn] },
                     op: LDAP::LDAP_MOD_ADD)
          rescue LDAP::Error => e
            warn e.full_message
            halt(500, "Failed to add user to communityMember group")
          end
        end

        begin
          request.status = 'approved'
          request.save
        rescue Sequel::ValidationFailed
          halt(500, "Failed to write request to database, but user has been created")
        end
      end

      def pending_requests
        Model::SignupRequest.where(status: 'pending').order(:created_at)
      end

      def resolved_requests
        Model::SignupRequest.exclude(status: 'pending').reverse(:resolved_at)
      end

      def signup_webhook
        webhook_text = "New signup request from *#{@signup_request.username}*.\n\n"
        if @signup_request.description && !@signup_request.description.empty?
          webhook_text << "```\n"
          webhook_text << @signup_request.description.gsub('```', '\\\\```').chomp
          webhook_text << "\n```\n\n"
        end
        webhook_text << "Go to https://rainbowcoalition.net/signups to view.\n"
        signup_webhook_post(webhook_text)
      end

      def new_request_email
        email_body = "New signup request from #{@signup_request.username}.\n\n"
        if @signup_request.description && !@signup_request.description.empty?
          email_body << @signup_request.description.gsub(/^/, "  ")
          email_body << "\n\n"
        end
        email_body << "Go to https://rainbowcoalition.net/signups to view.\n"

        Array(config.dig('signup', 'admin_notify_emails')).each do |admin_email|
          email(admin_email, "Signup Request from #{@signup_request.username}", email_body)
        end
      end

      def request_approved_email
        return unless @signup_request.email

        email_body = "Your signup request has been approved!\n\n"
        if @signup_request.reason && !@signup_request.reason.empty?
          email_body << @signup_request.reason.gsub(/^/, "  ")
          email_body << "\n\n"
        end
        email_body << "Go to https://rainbowcoalition.net/signup/#{@signup_request.token} to login " \
                      "and set up your profile."
        email(@signup_request.email, "Signup Request Approved", email_body)
      end

      def request_rejected_email
        return unless @signup_request.email

        email_body = "Your signup request has been rejected, but you may create another.\n\n"
        if @signup_request.reason && !@signup_request.reason.empty?
          email_body << @signup_request.reason.gsub(/^/, "  ")
          email_body << "\n\n"
        end
        email_body << "Usually, this is because your username is confusing or innapropriate.\n" \
                      "You may go to https://rainbowcoalition.net/signup to try again."
        email(@signup_request.email, "Signup Request Rejected", email_body)
      end

      def webhook_uri
        @webhook_uri ||= begin
          uri = config.dig('signup', 'webhook_uri')&.strip
          URI(uri.strip) if uri
        end
      end

      def signup_webhook_post(text)
        return unless webhook_uri
        Net::HTTP.post(webhook_uri, { text: text }.to_json, "Content-Type" => "application/json")
      rescue => e
        warn "Error when trying to post signup webhook:"
        warn e.full_message
      end
    end
  end

  class Application
    get '/signup' do
      extend Controllers::Signup

      if current_user
        redirect "/login", 303
      end

      if signup_token && !signup_request&.rejected?
        redirect "/signup/#{signup_token}", 303
      else
        slim :signup
      end
    end

    get '/signup/:token' do |token|
      extend Controllers::Signup

      @signup_request = Model::SignupRequest[token]

      if request.preferred_type(%w(text/html application/json)) == 'application/json'
        require 'json'

        return JSON.dump(@signup_request.to_hash.slice(:username, :description, :status, :reason))
      end

      unless @signup_request
        delete_secure_cookie '__Host-signup_token'
        redirect '/signup'
      end

      if signup_request.approved?
        expiry_days = config.dig('signup', 'expiry')&.to_i || 28
        # If older than four weeks, treat it as expired
        if (Time.now - @signup_request.resolved_at) > 60 * 60 * 24 * expiry_days
          delete_secure_cookie '__Host-signup_token'
          halt 404
        end

        do_login(signup_request.username)
        delete_secure_cookie '__Host-signup_token'
        redirect '/profile?redirect_to=intro', 303
      end

      slim(:signup)
    rescue LDAP::ResultError
      halt(404, slim(:signup))
    end

    post '/signup' do
      extend Controllers::Signup

      unless config['signup']&.fetch('enabled', true)
        post_errors['rejected'] << 'Signups temporarily disabled'
        halt(403, slim(:signup))
      end

      if signup_request&.status == 'hard_rejected'
        post_errors['rejected'] << signup_request.reason
        halt(403, slim(:signup))
      end

      params[:username]&.downcase!

      verify_params
      halt(400, slim(:signup)) unless post_errors.empty?

      token = nil
      Postgres.database.transaction do
        if user_exists?(params[:username])
          post_errors['username'] << 'Username taken'
          halt(409, slim(:signup))
        end

        token = generate_token
        @signup_request = Model::SignupRequest.new(
          token: token,
          username: params[:username],
          email: params[:email],
          description: params[:description],
          status: 'pending',
          created_at: Time.now
        )

        @signup_request.save
        set_secure_cookie("__Host-signup_token", token, same_site: :strict, expires: DateTime.now.next_year)
      end

      new_request_email
      signup_webhook

      redirect "/signup/#{token}", 303
    end

    get '/signups' do
      halt(404) unless current_user&.admin?
      extend Controllers::Signup

      slim(:signups)
    end

    post '/signups' do
      halt(404) unless current_user&.admin?
      extend Controllers::Signup

      token = params[:token]
      halt(400) unless token

      @signup_request = Model::SignupRequest[token]
      halt(404) unless @signup_request
      halt(400) unless ['approved', 'soft_rejected', 'hard_rejected'].include? params[:action]

      signup_request.resolved_at = Time.now
      signup_request.reason = params[:reason]

      if params[:action] == 'approved'
        approve_request(signup_request)
        request_approved_email
      else
        signup_request.status = params[:action]
        signup_request.save
        request_rejected_email if params[:action] == 'soft_rejected'
      end

      redirect "/signups", 303
    end

    get '/onboard' do
      halt 404 unless current_user&.admin?
      extend Controllers::Signup

      slim(:onboard)
    end

    post '/onboard' do
      halt 404 unless current_user&.admin?
      extend Controllers::Signup

      params[:username]&.downcase!
      post_errors['username'] << 'Invalid username' if !valid_username?(params[:username])
      halt(400, slim(:onboard)) unless post_errors.empty?

      token = nil
      create_for_existing = nil_s(params["existing"])
      Postgres.database.transaction do
        exists = user_exists?(params[:username])
        if create_for_existing && !exists
          post_errors['username'] << 'User not found'
          halt(409, slim(:onboard))
        elsif !create_for_existing && exists
          post_errors['username'] << 'Username taken'
          halt(409, slim(:onboard))
        end

        token = generate_token
        time = Time.now
        reason = params["existing"] ? 'Existing user sign-in link' : 'Preapproved onboarding'
        @signup_request = Model::SignupRequest.new(
          token: token,
          username: params[:username],
          reason: reason,
          status: 'approved',
          created_at: time,
          resolved_at: time
        )

        @signup_request.save
      end

      approve_request(@signup_request) unless create_for_existing
      slim(:onboard, {}, token: token)
    end
  end
end
