module RainbowCoalition
  class Application
    get '/md/*' do
      redirect "/login?redirect_to=#{Rack::Utils.escape(request.path)}", 303 unless current_user

      halt 404 if params['splat'].any? {|component| component.start_with?('.')}
      halt 404 if params['splat'].first == 'admin' && !current_user.admin?

      send_file(File.join(Application.root, "static", "restricted", *params["splat"]))
    end
  end
end
