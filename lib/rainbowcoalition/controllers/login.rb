require 'ldap'
require 'rainbowcoalition/ldap'

module RainbowCoalition
  module Controllers
    module Login
      def verify_username!(username)
        halt(403, slim(:login, {}, { error: 'Invalid username' })) unless valid_username?(username)
      end

      def bad_user_pass
        "Bad password"
      end
    end
  end

  class Application
    post '/login' do
      extend Controllers::Login

      # Verify username has reasonable characters
      username, password = params["username"], params["password"]
      verify_username! username

      authenticate_ldap_user!(username, password)
      do_login(username)
      redirect("/#{params['redirect_to']}", 303) if params['redirect_to']
      slim(:login, {}, { error: nil })
    rescue LDAP::ResultError
      halt(403, slim(:login, {}, { error: bad_user_pass }))
    end

    get '/login' do
      extend Controllers::Login

      redirect("/#{Rack::Utils.unescape(params['redirect_to'])}", 303) if params['redirect_to'] && current_user

      slim :login
    end

    post '/logout' do
      extend Controllers::Login

      do_logout

      slim(:login, {}, { error: nil })
    end

    post '/clear-sessions' do
      halt 404 unless current_user&.admin?

      clear_sessions(params['username'])

      redirect("/#{Rack::Utils.unescape(params['redirect_to'])}", 303) if params['redirect_to'] && current_user
      'Success'
    end
  end
end
