module RainbowCoalition
  module Controllers
    module Factorio
      BILL_OF_MATERIALS = {
        "blueprint" => {
          "label" => "Bill of Materials",
          "description" => 'Holds the items required to build all of the blueprints\' entities.',
          "icons" => [{"signal"=>{"type"=>"item", "name"=>"constant-combinator"}, "index"=>1}],
          "item"=>"blueprint", "version"=>281479276920832
        }
      }

      # Entities and tiles in Factorio are built from items.
      # For most entities, the name of the item matches the name
      # of the item. These are the exceptions
      #
      # function hasitem(table, element)
      #   for _, value in pairs(table) do
      #     if value.name == element then
      #       return true
      #     end
      #   end
      #   return false
      # end
      #
      # data = ""
      # for name, prototype in pairs(game.entity_prototypes) do
      #   items = prototype.items_to_place_this
      #   if (items ~= nil) and (next(items) ~= nil) and (not hasitem(items, name)) then
      #     data = data .. name .. " " .. items[1].name .. "\n"
      #   end
      # end

      ITEM_TABLE = {
        "bpsb-ils-se-core-miner-drill" => "se-core-miner",
        "bpsb-ils-se-delivery-cannon" => "se-delivery-cannon",
        "bpsb-ils-se-delivery-cannon-weapon" => "se-delivery-cannon-weapon",
        "bpsb-ils-se-energy-beam-defence" => "se-energy-beam-defence",
        "bpsb-ils-se-energy-transmitter-chamber" => "se-energy-transmitter-chamber",
        "bpsb-ils-se-energy-transmitter-emitter" => "se-energy-transmitter-emitter",
        "bpsb-ils-se-energy-transmitter-injector" => "se-energy-transmitter-injector",
        "bpsb-ils-se-meteor-defence-container" => "se-meteor-defence",
        "bpsb-ils-se-meteor-point-defence-container" => "se-meteor-point-defence",
        "bpsb-ils-se-rocket-landing-pad" => "se-rocket-landing-pad",
        "bpsb-ils-se-rocket-launch-pad" => "se-rocket-launch-pad",
        "bpsb-ils-se-space-elevator" => "se-space-elevator",
        "clay-bricks" => "clay-brick",
        "concrete-wall-ruin" => "concrete-wall",
        "curved-rail" => "rail",
        "dirt-1" => "landfill-dry-dirt",
        "dirt-2" => "landfill-dry-dirt",
        "dirt-3" => "landfill-dry-dirt",
        "dirt-4" => "landfill-dirt-4",
        "dirt-5" => "landfill-dirt-4",
        "dirt-6" => "landfill-dirt-4",
        "dirt-7" => "landfill-dirt-4",
        "dry-dirt" => "landfill-dry-dirt",
        "furnace-ruin" => "stone-furnace",
        "grass-1" => "landfill-grass-1",
        "grass-2" => "landfill-grass-1",
        "grass-3" => "landfill-grass-1",
        "grass-4" => "landfill-grass-1",
        "hazard-concrete-left" => "hazard-concrete",
        "hazard-concrete-right" => "hazard-concrete",
        "iron-wood-chest" => "iron-chest",
        "iron-wood-chest-remnants" => "iron-chest",
        "logistic-robot-dropped-cargo" => "logistic-robot",
        "red-desert-0" => "landfill-red-desert-1",
        "red-desert-1" => "landfill-red-desert-1",
        "red-desert-2" => "landfill-red-desert-1",
        "red-desert-3" => "landfill-red-desert-1",
        "red-inserter" => "long-handed-inserter",
        "refined-hazard-concrete-left" => "refined-hazard-concrete",
        "refined-hazard-concrete-right" => "refined-hazard-concrete",
        "rough-stone-path" => "stone",
        "sand-1" => "landfill-sand-3",
        "sand-2" => "landfill-sand-3",
        "sand-3" => "landfill-sand-3",
        "se-core-miner-drill" => "se-core-miner",
        "se-fuel-refinery-spaced" => "se-fuel-refinery",
        "se-meteor-defence-container" => "se-meteor-defence",
        "se-meteor-point-defence-container" => "se-meteor-point-defence",
        "se-space-assembling-machine-grounded" => "se-space-assembling-machine",
        "se-space-biochemical-laboratory-grounded" => "se-space-biochemical-laboratory",
        "se-space-curved-rail" => "se-space-rail",
        "se-space-decontamination-facility-grounded" => "se-space-decontamination-facility",
        "se-space-hypercooler-grounded" => "se-space-hypercooler",
        "se-space-laser-laboratory-grounded" => "se-space-laser-laboratory",
        "se-space-manufactory-grounded" => "se-space-manufactory",
        "se-space-mechanical-laboratory-grounded" => "se-space-mechanical-laboratory",
        "se-space-particle-accelerator-grounded" => "se-space-particle-accelerator",
        "se-space-radiation-laboratory-grounded" => "se-space-radiation-laboratory",
        "se-space-radiator-2-grounded" => "se-space-radiator-2",
        "se-space-radiator-grounded" => "se-space-radiator",
        "se-spaceship-circuit-network-restore" => "se-struct-generic-output",
        "se-spaceship-clamp-place" => "se-spaceship-clamp",
        "se-spaceship-clamp-power-pole-external-east" => "se-struct-generic-clamp-east",
        "se-spaceship-clamp-power-pole-external-west" => "se-struct-generic-clamp-west",
        "se-spaceship-console-output" => "se-struct-generic-output",
        "se-space-straight-rail" => "se-space-rail",
        "se-space-supercomputer-1-grounded" => "se-space-supercomputer-1",
        "se-space-supercomputer-2-grounded" => "se-space-supercomputer-2",
        "se-space-supercomputer-3-grounded" => "se-space-supercomputer-3",
        "se-space-supercomputer-4-grounded" => "se-space-supercomputer-4",
        "se-space-thermodynamics-laboratory-grounded" => "se-space-thermodynamics-laboratory",
        "steel-wall-ruin" => "steel-wall",
        "stone-path" => "stone-brick",
        "stone-rubble" => "stone-wall",
        "stone-wall-ruin" => "stone-wall",
        "straight-rail" => "rail",
        "tile-concrete-brick" => "concrete",
        "tile-reinforced-concrete-brick" => "refined-concrete",
        "vase" => "wood",
#        "water" => "blasting-charge",
        "water" => "bpsb-sbt-water",
        "wooden-barrel" => "wood",
        "wood-half-chest-left" => "wooden-chest",
        "wood-half-chest-right" => "wooden-chest",
        "workshop-ruin" => "assembling-machine-1"
      }

      def bill_of_materials(blueprint_string, **kwargs)
        require 'zlib'
        require 'json'
        require 'base64'

        data = Base64.decode64(blueprint_string[1..-1])

        contents = JSON.load(Zlib::Inflate.inflate(data))
        item_counts = Hash.new { 0 }

        blueprints, label = if contents["blueprint_book"]
          [contents.dig("blueprint_book", "blueprints"), contents.dig("blueprint_book", "label")]
        else
          [[contents], contents.dig("blueprint", "label")]
        end

        blueprints.each do |blueprint|
          entities = blueprint.dig("blueprint", "entities")
          entities&.each do |entity|
            item_name = ITEM_TABLE.fetch(entity["name"], entity["name"])
            item_counts[item_name] += 1
            entity["items"]&.each_pair do |item, count|
              item_counts[item] += count
            end
          end

          if kwargs[:tiles]
            blueprint.dig("blueprint", "tiles")&.
              map {|tile| tile["name"]}&.
              each do |tile|
              item_counts[ITEM_TABLE.fetch(tile, tile)] += 1
            end
          end
        end

        num_slices = (item_counts.keys.size + 19) / 20

        combinators = item_counts.
          each_pair.
          to_a.
          sort_by {|item, count| -count}.
          each_slice(20). # 20 is the signal limit for combinators
          each_with_index.
          map do |slice, index|
          filters = slice.each_with_index.map do |x, i|
            item, count = x

            {
              "signal" => {
                "type" => "item",
                "name" => item
              },
              "count" => count,
              "index" => i + 1
            }
          end

          connections = []
          connections << { "entity_id": index } unless index == 0
          connections << { "entity_id": index + 2 } unless index == (num_slices - 1)

          {
            "entity_number" => index + 1,
            "name" => "constant-combinator",
            "position" => {
              "x" => 0.5 + index,
              "y" => 0.5
            },
            "control_behavior" => {
              "filters" => filters
            },
            "connections" => {
              "1": {
                "red": connections
              }
            }
          }
        end

        BILL_OF_MATERIALS.dup.tap {|h|
          h["blueprint"]["entities"] = combinators

          if label
            h["blueprint"]["description"] = "Holds the items required to build all of the entities in '#{label}'."
            h["blueprint"]["label"] = "Bill of Materials (#{label})"
          end
        }.then {|b|
          JSON.dump(b)
        }.then {|b|
          Zlib::Deflate.deflate(b)
        }.then  {|b|
          Base64.encode64(b)
        }.then {|b|
          "0" + b
        }
      end
    end
  end

  class Application
    get '/factorio/bom' do
      extend Controllers::Factorio

      slim(:bom, {}, blueprint_string: '')
    end

    post '/factorio/bom' do
      extend Controllers::Factorio

      begin
        output_string = bill_of_materials(params["blueprint_string"], tiles: params["tiles"])

        slim(:bom, {}, blueprint_string: output_string)
      rescue => e
        post_errors["blueprint_string"] << "Error parsing blueprint string"

        warn e

        halt(400, slim(:bom, {}, blueprint_string: params["blueprint_string"]))
      end
    end
  end
end
