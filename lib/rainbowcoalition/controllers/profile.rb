require 'rainbowcoalition/magick'

module RainbowCoalition
  module Controllers
    module Profile
      def verify_profile_params(params = self.params)
        if params['display-name'] && params['display-name'].size > 64
          post_errors['display-name'] << 'Display name can be at most 64 characters long'
        end

        unless params['password'].nil? || params['password'].empty?
          if params['password'] != params['password-verify']
            post_errors['password-verify'] << 'Passwords do not match'
          end
          if params['password'].size > 512
            post_errors['password'] << 'Password too large'
          end

          if params['password'].size < 10
            post_errors['password'] << 'Password too short'
          end
        end

        if params['name'] && params['name'].size > 128
          post_errors['name'] << 'Name can be at most 128 characters long'
        end

        if params['description'] && params['description'].size > 16384
          post_errors['description'] << 'Description can be at most 16kB long'
        end

        if params['email']
          post_errors['email'] << 'Email too large' if params['email'].size > 1024
          post_errors['email'] << 'Not an email' if !params['email'].empty? && !params['email'].include?('@')
        end
      end

      def update_user_profile(params = self.params)
        require 'rainbowcoalition/password'
        require 'rainbowcoalition/user'

        new_user = target_user_profile.dup
        unless params['password'].nil? || params['password'].empty?
          new_user.user_password = Password.ldap_password(params['password'])
        end

        name = params['name'] || new_user.cn
        display_name = params['display-name'] || new_user.display_name
        mail = params['email'] || new_user.mail
        description = params['description'] || new_user.description
        title = params['title'] || new_user.title

        if name.empty?
          new_user.cn = new_user.uid
        else
          new_user.cn = name
        end
        new_user << {
          display_name: nil_s(display_name),
          mail: nil_s(mail),
          description: nil_s(description)
        }

        if @face
          new_user << { jpeg_photo: @face[:jpg] }
        end

        if current_user.admin?
          new_user << { title: nil_s(title) }
        end

        with_ldap {|l| l.update(target_user_profile, new_user)}

        if profile_self?
          refresh_current_user
        else
          @user_profile = nil
        end
      end

      def profile_uid
        @target_uid || current_user.uid
      end

      def profile_self?
        profile_uid == current_user.uid
      end

      def target_user_profile
        return current_user if profile_self?

        @user_profile ||= begin
          if @public_target
            public_user_profile(profile_uid)
          else
            full_user_profile(profile_uid)
          end
        end
      end

      def profile_input(name, label, type = 'text', ldap_field = name.tr('-', '_'), **kwargs)
        if ldap_field
          kwargs[:value] ||= target_user_profile.public_send(ldap_field)
        else
          kwargs[:value] = ''
        end

        input(name, label, type, nil, **kwargs)
      end

      def user_sort(users)
        # TODO: Implement international sorting
        # libicu / icu-ffi
        Array(users).sort_by {|user| user&.friendly_name&.downcase}
      end

      def process_face
        if params.dig('face', 'tempfile')
          file = params.dig('face', 'tempfile')
          magick_result = Magick.to_face(file.read)

          if magick_result[:error]
            post_errors['face'] << "Image is invalid. Please upload a JPEG or PNG."
          else
            @face = magick_result
          end
        end
      rescue
        halt(400, slim(:profile))
      end
    end
  end

  class Application
    get '/profile' do
      extend Controllers::Profile

      redirect "/login?redirect_to=profile", 303 unless current_user

      return_code = current_user.nil? ? 401 : 200
      [return_code, slim(:profile)]
    end

    post '/profile/:uid' do |uid|
      halt 403 unless current_user&.admin?
      extend Controllers::Profile

      @target_uid = uid
      verify_profile_params
      process_face

      unless post_errors.empty?
        halt(400, slim(:profile))
      end

      update_user_profile

      slim :profile
    end

    post '/profile' do
      extend Controllers::Profile

      halt 401 unless current_user

      verify_profile_params
      process_face

      unless post_errors.empty?
        halt(400, slim(:profile))
      end

      update_user_profile

      redirect("/#{Rack::Utils.unescape(params['redirect_to'])}", 303) if params['redirect_to']

      slim :profile
    end

    get '/profile/:uid' do |uid|
      halt 401 unless current_user
      extend Controllers::Profile

      @target_uid = uid
      if current_user.admin?
        halt 404 unless target_user_profile

        slim(:profile)
      else
        if profile_self?
          slim(:profile)
        else
          @public_target = true
          halt 404 unless target_user_profile

          slim(:profile_public)
        end
      end
    end

    get '/profiles' do
      redirect "/login?redirect_to=profiles", 303 unless current_user

      extend Controllers::Profile

      slim :profiles
    end

    get '/profiles/:uid/face.jpg' do |uid|
      halt 401 unless current_user
      extend Controllers::Profile

      unless profile_self? || current_user.admin?
        @public_target = true
      end

      @target_uid = uid

      halt 404 unless target_user_profile&.jpeg_photo
      [200, {'Content-Type' => 'image/jpeg'}, target_user_profile.jpeg_photo]
    end
  end
end
