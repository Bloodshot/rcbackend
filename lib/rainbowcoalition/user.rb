require 'rainbowcoalition/model/session_token'

module RainbowCoalition
  module User
    LDAP_USERS = {}

    def current_user
      @current_user ||= lookup_user_by_session_token(session_token) if session_token
    end

    def session_token
      secure_cookie '__Secure-session_token'
    end

    def session_duration
      config.dig('user', 'session_timeout') || 28
    end

    def refresh_current_user
      return unless @current_user
      fetch_user_from_ldap

      @current_user = nil
      current_user
    end

    def lookup_user_by_session_token(session_token)
      token = lookup_session_token(session_token)
      return unless token
      if token.expired?
        token.delete
        return
      end

      token.last_used = Model::SessionToken.now
      token.save

      uid = token.user

      lookup_user(uid)
    rescue Sequel::Error => e
    end

    def lookup_user(uid)
      modtime, user = LDAP_USERS[uid]
      if !user || (Time.now - modtime > 120)
        fetch_user_from_ldap(uid)[1]
      else
        user
      end
    end

    def lookup_session_token(session_token)
      Model::SessionToken[session_token]
    end

    def fetch_user_from_ldap(uid = @current_user.uid)
      LDAP_USERS[uid] = [Time.now, user_profile(uid)]
    end

    def do_login(username)
      @current_user = lookup_user(username.downcase)
      session_token = Model::SessionToken.create_for(username.downcase).token
      set_secure_cookie("__Secure-session_token", session_token, expires: DateTime.now + session_duration)
    end

    def do_logout
      @current_user = nil
      Model::SessionToken.where(token: session_token).delete if session_token
      delete_secure_cookie('__Secure-session_token')
    end

    def clear_sessions(username)
      Model::SessionToken.where(user: username).destroy
      Model::SignupRequest.where(username: username, status: 'approved').destroy
    end

    def valid_username?(username)
      username && !username.empty? && username.size < 80 &&
        /^[a-zA-Z0-9_]*$/.match(username)
    end
  end
end
