require 'ldap'
require 'rainbowcoalition/model/user'

module RainbowCoalition
  module Ldap
    SCOPE_MAP = {
      exact: LDAP::LDAP_SCOPE_BASE,
      base: LDAP::LDAP_SCOPE_BASE,
      onelevel: LDAP::LDAP_SCOPE_ONELEVEL,
      one: LDAP::LDAP_SCOPE_ONELEVEL,
      subtree: LDAP::LDAP_SCOPE_SUBTREE
    }

    def self.ldap_uri
      @ldap_uri ||= (RainbowCoalition::Application.config.dig('ldap', 'ldap_uri') || 'ldapi:///')
    end

    def Ldap.ldap_base_dn
      @ldap_base_dn ||= (RainbowCoalition::Application.config.dig('ldap', 'base_dn') || 'dc=rainbowcoalition,dc=net')
    end

    def Ldap.ldap_people_dn
      @ldap_people_dn ||= "#{RainbowCoalition::Application.config.dig('ldap', 'people_dn') || 'ou=people'},#{ldap_base_dn}"
    end

    def Ldap.ldap_group_dn
      @ldap_group_dn ||= "#{RainbowCoalition::Application.config.dig('ldap', 'group_dn') || 'ou=groups'},#{ldap_base_dn}"
    end

    def ldap
      connection = LDAP::Conn.open_uri(Ldap.ldap_uri)
      connection.set_option(LDAP::LDAP_OPT_PROTOCOL_VERSION, 3)
      connection.sasl_quiet = true
      connection
    end

    def authenticate_ldap_user!(username, password)
      halt 401 unless ldap.bind("uid=#{username},#{Ldap.ldap_people_dn}", password)
    end

    def with_ldap(uid = nil, &blk)
      connection = ldap

      uid ||= current_user&.uid
      halt 401 unless uid

      connection.sasl_bind('', 'external')

      if block_given?
        begin
          yield LdapConnection.new(connection, uid)
        ensure
          connection.unbind if connection.bound?
        end
      else
        connection
      end
    end

    def with_ldap_admin(&blk)
      connection = ldap

      connection.sasl_bind('', 'external')

      if block_given?
        begin
          yield LdapConnection.new(connection, nil)
        ensure
          connection.unbind if connection.bound?
        end
      else
        connection
      end
    end

    def self.ldap_escape(filter)
      filter.gsub(/[\0*()\\]/, {
        '\\' => '\5C',
        "\0" => '\00',
        '*'  => '\2A',
        '('  => '\28',
        ')'  => '\29'
      })
    end

    def ldap_escape(filter)
      Ldap.ldap_escape(filter)
    end

    def ldap_user_attribute(attribute_name, username = current_user.username)
      with_ldap do |l|
        results = l.search(Ldap.ldap_people_dn, "uid=#{ldap_escape(username)}", attributes: attribute_name, size: 1)
        results[0][attribute_name][0]
      end
    end

    def user_profile(uid)
      with_ldap(uid) do |l|
        l.search("uid=#{ldap_escape(uid)},#{Ldap.ldap_people_dn}",
                 'objectClass=*',
                 attributes: Model::User.ldap_attribute_names,
                 scope: :exact,
                 size: 1
                )
         .then do |results|
           break nil if results.empty?
           Model::User.new(results.first)
        end
      end
    end

    def public_user_profile(uid)
      with_ldap do |l|
        l.search("uid=#{ldap_escape(uid)},#{Ldap.ldap_people_dn}",
                 "memberOf=cn=communityMember,#{Ldap.ldap_group_dn}",
                 attributes: Model::User.public_user_attributes,
                 scope: :exact,
                 size: 1
                )
         .then do |results|
           break nil if results.empty?
           Model::User.new(results.first)
        end
      rescue LDAP::ResultError
        nil
      end
    end

    def full_user_profile(uid)
      with_ldap do |l|
        l.search("uid=#{ldap_escape(uid)},#{Ldap.ldap_people_dn}",
                 'objectClass=*',
                 attributes: Model::User.ldap_attribute_names,
                 scope: :exact,
                 size: 1
                )
         .then do |results|
           break nil if results.empty?
           Model::User.new(results.first)
        end
      rescue LDAP::ResultError
        nil
      end
    end

    def all_user_profiles
      with_ldap do |l|
        l.search(Ldap.ldap_people_dn,
                 "uid=*",
                 attributes: Model::User.ldap_attribute_names,
                ).map do |user|
          Model::User.new(user)
        end
      end
    end

    def public_user_profiles
      with_ldap do |l|
        l.search(Ldap.ldap_people_dn,
                 "(&(memberOf=cn=communityMember,#{Ldap.ldap_group_dn})" \
                 "(!(memberOf=cn=inactive,#{Ldap.ldap_group_dn}))" \
                 ")",
                 attributes: Model::User.public_user_attributes
                ).map do |user|
          Model::User.new(user)
        end
      end
    end

    class LdapConnection
      attr_reader :uid

      def initialize(ldap, uid)
        @ldap = ldap
        @uid = uid
      end

      def search(base, filter, **kwargs)
        scope = kwargs[:scope]
        scope = SCOPE_MAP[scope] if scope.is_a? Symbol
        scope ||= LDAP::LDAP_SCOPE_ONELEVEL

        server_controls = kwargs.fetch(:server_controls, [])
        if @uid
          server_controls += [proxy_auth_control]
        end


        @ldap.search_ext2(base,
                          scope,
                          filter,
                          kwargs.fetch(:attributes, nil),
                          kwargs.fetch(:attributes_only, false),
                          server_controls,
                          kwargs.fetch(:client_controls, nil),
                          kwargs.fetch(:timeout_sec, 0),
                          kwargs.fetch(:timeout_usec, 0),
                          kwargs.fetch(:size, 0),
                         )
      end

      private def proxy_auth_control
        LDAP::Control.new("2.16.840.1.113730.3.4.18",
                          "dn:uid=#{Ldap.ldap_escape(@uid)},#{Ldap.ldap_people_dn}",
                          true)
      end

      def update(from, to, **kwargs)
        raise "DNs must match" unless from.dn == to.dn

        from_h = from.to_h
        to_h = to.to_h
        mods = []

        to_h.each_pair do |key, value|
          next if key == :dn

          from_value = from_h[key]
          next if value == from_value

          bvalue = to.class.binary_attribute_names.include?(key) ? LDAP::LDAP_MOD_BVALUES : 0
          ldap_attribute = to.class.attribute_name_map[key].to_s

          mods << LDAP.mod(LDAP::LDAP_MOD_REPLACE | bvalue, ldap_attribute, Array(value))
        end

        modify(to.dn, mods, **kwargs)
      end

      def modify(dn, mods, **kwargs)
        mods = LDAP.hash2mods(kwargs.fetch(:op, LDAP::LDAP_MOD_REPLACE), mods) if mods.is_a? Hash

        server_controls = kwargs.fetch(:server_controls, [])
        if @uid
          server_controls += [proxy_auth_control]
        end

        @ldap.modify_ext(dn, mods, server_controls, kwargs.fetch(:client_controls, []))
      end

      def create(model, **kwargs)
        mods = []

        model.to_h.each_pair do |key, value|
          next if key == :dn
          next unless value

          bvalue = model.class.binary_attribute_names.include?(key) ? LDAP::LDAP_MOD_BVALUES : 0
          ldap_attribute = model.class.attribute_name_map[key].to_s

          mods << LDAP.mod(LDAP::LDAP_MOD_ADD | bvalue, ldap_attribute, Array(value))
        end

        add(model.dn, mods)
      end

      def add(dn, mods, **kwargs)
        mods = LDAP.hash2mods(LDAP::LDAP_MOD_ADD, mods) if mods.is_a? Hash

        server_controls = kwargs.fetch(:server_controls, [])
        if @uid
          server_controls += [proxy_auth_control]
        end

        @ldap.add_ext(dn, mods, server_controls, kwargs.fetch(:client_controls, []))
      end
    end
  end
end
