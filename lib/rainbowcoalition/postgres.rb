require 'sequel'

module RainbowCoalition
  module Postgres

    Sequel.extension :migration

    def self.database
      @@database ||= begin
        db = Sequel.postgres(
          database: (RainbowCoalition::Application.config.dig('database', 'name') || 'rcbackend'),
          user: RainbowCoalition::Application.config.dig('database', 'user'),
          password: RainbowCoalition::Application.config.dig('database', 'password')
        )

        db.extension :pg_enum, :connection_validator

        db
      end
    end
  end
end
