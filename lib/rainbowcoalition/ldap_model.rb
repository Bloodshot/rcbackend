require 'rainbowcoalition/model'

module RainbowCoalition
  class LdapModel < Model
    private_class_method def self.inherited(model)
      super(model)

      model.class_exec do
        @binary_attributes = []
        @array_attributes = []

        attributes :dn
        must_have :dn
        array_attributes :objectClass
      end
    end

    private_class_method def self.binary_attributes(*attributes)
      attributes = coerce(attributes)
      attributes(attributes - @attribute_name_map.values)
      @binary_attributes |= attributes.map {|attribute| snake_case(attribute).to_sym}
    end

    private_class_method def self.array_attributes(*attributes)
      attributes = coerce(attributes)
      attributes(attributes - @attribute_name_map.values)
      @array_attributes |= attributes.map {|attribute| snake_case(attribute).to_sym}
    end

    def self.binary_attribute_names
      @binary_attributes
    end

    def self.array_attribute_names
      @array_attributes
    end

    def self.ldap_attribute_names
      attributes.map {|attribute| @attribute_name_map[attribute].to_s}
    end

    def initialize(hash = nil, **kwargs)
      raise ArgumentError if (hash && (!kwargs.empty?))
      hash ||= kwargs

      transformed = hash.each_pair.map do |k, v|
        k = Model.snake_case(k).to_sym
        [k, transform_ldap_value(k, v)]
      end

      super(Hash[transformed])
    end

    private def transform_ldap_value(attribute, value)
      value = if array_attributes.include?(attribute)
        Array(value)
      elsif value.is_a? Array
        raise ValidationError, "Invalid multi-valued attribute #{attribute}: #{value.inspect}" if value.size > 1

        value.first
      else
        value
      end

      unless binary_attributes.include? attribute
        Array(value).each {|string| string.force_encoding(Encoding::UTF_8)}
      end

      value
    end

    private def validate_and_set(attribute, value)
      super(attribute, transform_ldap_value(attribute, value))
    end

    def binary_attributes
      self.class.binary_attribute_names
    end

    def array_attributes
      self.class.array_attribute_names
    end

    def to_s
      attr_string = (attributes - binary_attributes).
        select {|attr| !send(attr).nil? }.
        map {|attr| "#{attr}: #{send(attr)}" }.
        join(', ')
      "#<#{self.class.name} #{attr_string}>"
    end

    def inspect
      to_s
    end
  end
end
