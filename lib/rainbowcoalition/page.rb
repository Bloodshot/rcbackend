require 'kramdown'

module RainbowCoalition
  class Page
    attr_accessor :file, :name, :title, :access
    attr_reader :body

    def initialize(file, name)
      @file = file
      @name = name

      render!
    end

    # Sugar methods for use in templates,
    # Since title = 'whatever' gets interpreted as a local

    def title(arg = nil)
      self.title = arg if arg
      @title
    end

    def access(arg = nil)
      self.access = arg if arg
      @access
    end

    def render!
      @rendered_at = Time.now
      @body = Tilt.new(@file).render(self)
    end

    def right_sidebar(&blk)
      # Hack to get the block to *not* append to the document
      if block_given?
        old_buffer = blk.binding.local_variable_get(:_buf)
        blk.binding.local_variable_set(:_buf, '')
        @right_sidebar = yield
        blk.binding.local_variable_set(:_buf, old_buffer)
      end

      @right_sidebar
    end

    def sidebar_target(group = nil)
      @sidebar = group if group
      @sidebar
    end

    def link(title, target, access = nil)
      (RainbowCoalition::Application::SIDEBARS["/#{name}"] ||= []) << [title, target, access]
    end

    def template(name, options = {}, locals, &blk)
      Tilt.new("#{Application.root}/templates/#{name}.slim", nil, options, &blk).render(self, locals)
    end
  end
end
