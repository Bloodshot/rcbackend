Sequel.migration do
  change do
    create_table(:session_tokens) do
      column(:token, 'char(40)', primary_key: true)
      column(:user, String, null: false)
      column(:created_at, 'timestamp (2)', null: false)
      column(:last_used, 'timestamp (2)', null: false)
    end
  end
end
