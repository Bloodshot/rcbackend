Sequel.migration do
  up do
    extension :pg_enum

    create_enum(:signup_status, %w(pending approved soft_rejected hard_rejected))

    create_table(:signup_requests) do
      column(:token, 'char(48)', primary_key: true)
      column(:username, 'varchar(128)', null: false)
      column(:description, String)
      column(:email, String)
      column(:status, 'signup_status', null: false)
      column(:reason, String)
      column(:created_at, 'timestamp (2)', null: false)
      column(:resolved_at, 'timestamp (2)')
    end
  end

  down do
    extension :pg_enum

    drop_table(:signup_requests)
    drop_enum(:signup_status)
  end
end
